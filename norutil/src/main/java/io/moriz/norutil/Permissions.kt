package io.moriz.norutil

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


class Permissions {
    //需要申请权限的数组
    private val permissions = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.MANAGE_EXTERNAL_STORAGE
//        Manifest.permission.ACCESS_FINE_LOCATION,
//        Manifest.permission.BLUETOOTH_ADMIN,
//        Manifest.permission.BLUETOOTH_CONNECT,
//        Manifest.permission.BLUETOOTH,
//        Manifest.permission.BLUETOOTH_SCAN
    )

    //保存真正需要去申请的权限
    private val permissionList: MutableList<String> = ArrayList()
    fun checkPermissions(activity: Activity?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (i in permissions.indices) {
                if (ContextCompat.checkSelfPermission(
                        activity!!,
                        permissions[i]
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    permissionList.add(permissions[i])
                }
            }
            //有需要去动态申请的权限
            if (permissionList.size > 0) {
                requestPermission(activity)
            }
        }
    }

    //去申请的权限
    fun requestPermission(activity: Activity?) {
        ActivityCompat.requestPermissions(activity!!, permissionList.toTypedArray(), RequestCode)
    }

    companion object {
        var RequestCode = 100
    }
}