package io.moriz.norutil

import android.content.Context
import android.view.Gravity
import android.widget.Toast


fun showToast(ctx: Context, id: Int, str: String?) {
    if (str == null) {
        return
    }
    val toast: Toast = Toast.makeText(ctx, ctx.getString(id) + str, Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

fun showToast(ctx: Context?, errInfo: String?) {
    if (errInfo == null) {
        return
    }
    val toast = Toast.makeText(ctx, errInfo, Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}