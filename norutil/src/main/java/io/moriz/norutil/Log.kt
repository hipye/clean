package io.moriz.norutil

import android.util.Log

object L {
    //TAG
    var TAG = "日志提醒"

    //5个等级 DIWE
    fun d(text: String) {
        Log.d(TAG, text + "")
    }

    fun i(text: String) {
        Log.i(TAG, text + "")
    }

    fun w(text: String) {
        Log.w(TAG, text + "")
    }

    fun e(text: String) {
        Log.e(TAG, text + "")
    }
}