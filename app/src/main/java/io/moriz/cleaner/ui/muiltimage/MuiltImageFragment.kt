package io.moriz.cleaner.ui.muiltimage

import ando.file.core.FileGlobal
import ando.file.core.FileLogger
import ando.file.core.FileSizeUtils
import ando.file.core.FileUtils
import ando.file.selector.*
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import io.moriz.cleaner.*
import io.moriz.cleaner.R
import io.moriz.cleaner.ui.selector.FileSelectFragment
import io.moriz.cleaner.ui.selector.FileSelectResultAdapter
import io.moriz.cleaner.utils.PermissionManager
import io.moriz.cleaner.utils.ResultUtils
import io.moriz.cleaner.utils.ResultUtils.asVerticalList
import java.io.File

class MuiltImageFragment : Fragment() {

    companion object {
        private const val REQUEST_CHOOSE_IMAGE = 10
        private const val REQUEST_CHOOSE_FILE = 11

        fun newInstance(): FileSelectFragment {
            val args = Bundle()
            val fragment = FileSelectFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val mShowText: String by lazy { getString(R.string.str_ando_file_select_multiple_and_compress) }
    private lateinit var mTvCurrStrategy: TextView
    private lateinit var mRgStrategy: RadioGroup
    private lateinit var mBtSelect: Button
    private lateinit var mTvError: TextView
    private lateinit var mRvResults: RecyclerView

    private var mResultShowList: MutableList<ResultUtils.ResultShowBean>? = null
    private val mAdapter: FileSelectResultAdapter by lazy { FileSelectResultAdapter() }
    private var mFileSelector: FileSelector? = null

    private var mOverLimitStrategy: Int = FileGlobal.OVER_LIMIT_EXCEPT_ALL

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.activity_select_multi_files, container, false)
        mTvCurrStrategy = v.findViewById(R.id.tv_curr_strategy)
        mTvError = v.findViewById(R.id.tv_error)
        mRgStrategy = v.findViewById(R.id.rg_strategy)
        mBtSelect = v.findViewById(R.id.bt_select_multi)
        mRvResults = v.findViewById(R.id.rv_images)
        mRvResults.asVerticalList()
        mRvResults.adapter = mAdapter

        //策略切换 (Strategy switching)
        val prefix = getString(R.string.str_ando_file_current_strategy)
        mRgStrategy.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_strategy1 -> {
                    this.mOverLimitStrategy = FileGlobal.OVER_LIMIT_EXCEPT_ALL
                    mTvCurrStrategy.text = "$prefix 异常模式"
                }
                R.id.rb_strategy2 -> {
                    this.mOverLimitStrategy = FileGlobal.OVER_LIMIT_EXCEPT_OVERFLOW
                    mTvCurrStrategy.text = "$prefix 正常模式"
                }
                else -> {
                }
            }
        }
        mTvCurrStrategy.text = "$prefix ${
            if (this.mOverLimitStrategy == FileGlobal.OVER_LIMIT_EXCEPT_ALL) "异常模式"
            else "正常模式"
        }"

        mBtSelect.text = "$mShowText (0)"
        mBtSelect.setOnClickListener {
            PermissionManager.requestStoragePermission(this) {
                if (it) chooseFile()
            }
        }
        return v//返回定义的结果与前面的"val v"对应!!必须有  所有控件定义必须在这个之前
    }

    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ResultUtils.resetUI(mTvError)
        mFileSelector?.obtainResult(requestCode, resultCode, data)
    }

    private fun chooseFile() {
        val optionsImage = FileSelectOptions().apply {
            fileType = FileType.IMAGE
            fileTypeMismatchTip = "File type mismatch !"
            singleFileMaxSize = 5242880
            singleFileMaxSizeTip = "A single picture does not exceed 5M !"
            allFilesMaxSize = 10485760
            allFilesMaxSizeTip = "The total size of the picture does not exceed 10M !"
            fileCondition = object : FileSelectCondition {
                override fun accept(fileType: IFileType, uri: Uri?): Boolean {
                    return (fileType == FileType.IMAGE && uri != null && !uri.path.isNullOrBlank() && !FileUtils.isGif(uri))
                }
            }
        }

        mFileSelector = FileSelector
            .with(this)
            .setRequestCode(io.moriz.cleaner.REQUEST_CHOOSE_FILE)
            .setMultiSelect()
            .setMinCount(1, "Choose at least one picture!")
            .setMaxCount(10, "Choose up to ten pictures!")
            .setSingleFileMaxSize(3145728, "The size of a single picture cannot exceed 3M !")
            .setAllFilesMaxSize(20971520, "The total size of the picture does not exceed 20M !")
            .setOverLimitStrategy(this.mOverLimitStrategy)
            .setExtraMimeTypes("image/*")
            .applyOptions(optionsImage)
            .filter(object : FileSelectCondition {
                override fun accept(fileType: IFileType, uri: Uri?): Boolean {
                    return (fileType == FileType.IMAGE) && (uri != null && !uri.path.isNullOrBlank() && !FileUtils.isGif(uri))
                }
            })
            .callback(object : FileSelectCallBack {
                override fun onSuccess(results: List<FileSelectResult>?) {
                    FileLogger.w("FileSelectCallBack onSuccess ${results?.size}")
                    mAdapter.setData(null)
                    if (results.isNullOrEmpty()) {
//                        toastLong("No file selected")
                        return
                    }
                    showSelectResult(results)
                }

                override fun onError(e: Throwable?) {
                    FileLogger.e("FileSelectCallBack onError ${e?.message}")
                    ResultUtils.setErrorText(mTvError, e)

                    mAdapter.setData(null)
                    mBtSelect.text = "$mShowText (0)"
                }
            })
            .choose()
    }

    private fun showSelectResult(results: List<FileSelectResult>) {
        ResultUtils.setErrorText(mTvError, null)
        mBtSelect.text = "$mShowText (${results.size})"
        ResultUtils.formatResults(results, true) { l ->
            mResultShowList = l.map { p ->
                ResultUtils.ResultShowBean(originUri = p.first, originResult = p.second)
            }.toMutableList()
        }

        //List<FileSelectResult> -> List<Uri>
        val photos: List<Uri> = results
            .filter { (it.uri != null) && (FileType.INSTANCE.fromUri(it.uri) == FileType.IMAGE) }
            .map {
                it.uri!!
            }

        var count = 0
        compressImage(requireActivity(), photos) { index, u ->
            FileLogger.i("compressImage onSuccess index=$index uri=$u " +
                    "${getString(R.string.str_ando_file_compress_dir_size)} = ${FileSizeUtils.getFolderSize(File(getCompressedImageCacheDir()))}"
            )

            ResultUtils.formatCompressedImageInfo(u, true) {
                if (index != -1) {
                    mResultShowList?.get(index)?.compressedResult = it
                    mResultShowList?.get(index)?.compressedUri = u
                    count++
                }
            }

            //建议加个加载中的弹窗 (It is recommended to add a loading dialog)
            if (count == mResultShowList?.size ?: 0) {
                mAdapter.setData(mResultShowList)
            }
        }
    }


}//end