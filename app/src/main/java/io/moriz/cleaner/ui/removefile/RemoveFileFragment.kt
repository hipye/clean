package io.moriz.cleaner.ui.removefile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import io.moriz.cleaner.R
import io.moriz.norutil.UseFileUtils
import io.moriz.norutil.showToast

class RemoveFileFragment : Fragment() {

    private lateinit var mRmfButton: Button
    private lateinit var mFilesCheckbox: CheckBox
    private lateinit var mFilesCheckbox2: CheckBox
    private lateinit var mFilesCheckbox3: CheckBox
    private lateinit var mFilesCheckbox4: CheckBox
    private lateinit var mFilesCheckbox5: CheckBox
    private lateinit var mFilesCheckbox6: CheckBox
    private lateinit var mFilesCheckbox7: CheckBox
    private lateinit var mFilesCheckbox8: CheckBox
    private lateinit var mFilesCheckbox9: CheckBox
    private lateinit var mFilesCheckbox10: CheckBox
    private lateinit var mFilesCheckbox11: CheckBox
    private lateinit var mFilesCheckbox12: CheckBox
    private lateinit var mFilesCheckbox13: CheckBox
    private lateinit var mFilesCheckbox14: CheckBox
    private lateinit var mFilesCheckbox15: CheckBox
    private lateinit var mFilesCheckbox16: CheckBox
    private lateinit var mFilesCheckbox17: CheckBox
    private lateinit var mFilesCheckbox18: CheckBox
    private lateinit var mFilesCheckbox19: CheckBox
    private lateinit var mFilesCheckbox20: CheckBox

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val tv = inflater.inflate(R.layout.fragment_home,container,false)
        onResume()
        //getRoot()

        mRmfButton = tv.findViewById(R.id.RMF_button)
        mRmfButton.setOnClickListener {
            showToast(requireActivity(),"删除垃圾文件")
            UseFileUtils.delDir("/storage/emulated/0/.com.taobao.dp")
            UseFileUtils.delDir("/storage/emulated/0/.zp")
            UseFileUtils.delDir("/storage/emulated/0/OSSLog")
            UseFileUtils.delDir("/storage/emulated/0/.gs_fs6")
            UseFileUtils.delDir("/storage/emulated/0/.gs_fs0")
            UseFileUtils.delDir("/storage/emulated/0/.7934039a")
            UseFileUtils.delDir("/storage/emulated/0/.OAIDSystemConfig")
            UseFileUtils.delDir("/storage/emulated/0/.UTSystemConfig")
            UseFileUtils.delDir("/storage/emulated/0/.BD_SAPI_CACHE")
            UseFileUtils.delDir("/storage/emulated/0/TurbonetDiskCache")
            UseFileUtils.delDir("/storage/emulated/0/TurboNet")
            UseFileUtils.delDir("/storage/emulated/0/.seprivate")
            UseFileUtils.delDir("/storage/emulated/0/.vdevdir")
            UseFileUtils.delDir("/storage/emulated/0/com.quark.browser")
            UseFileUtils.delDir("/storage/emulated/0/com.baidu.netdisk")
            UseFileUtils.delDir("/storage/emulated/0/alipay")
            UseFileUtils.delDir("/storage/emulated/0/geetest")
            UseFileUtils.delDir("/storage/emulated/0/ttscache")
            //UseFileUtils.delDir("/storage/emulated/0/Android/data/org.telegram.messenger.web/files/Telegram/Telegram Video")
            //UseFileUtils.delDir("/storage/emulated/0/Android/data/org.telegram.messenger.web/files/Telegram/Telegram Images")
            onResume()
        }

        mFilesCheckbox = tv.findViewById(R.id.files_checkBox)
        mFilesCheckbox2 = tv.findViewById(R.id.files_checkBox2)
        mFilesCheckbox3 = tv.findViewById(R.id.files_checkBox3)
        mFilesCheckbox4 = tv.findViewById(R.id.files_checkBox4)
        mFilesCheckbox5 = tv.findViewById(R.id.files_checkBox5)
        mFilesCheckbox6 = tv.findViewById(R.id.files_checkBox6)
        mFilesCheckbox7 = tv.findViewById(R.id.files_checkBox7)
        mFilesCheckbox8 = tv.findViewById(R.id.files_checkBox8)
        mFilesCheckbox9 = tv.findViewById(R.id.files_checkBox9)
        mFilesCheckbox10 = tv.findViewById(R.id.files_checkBox10)
        mFilesCheckbox11 = tv.findViewById(R.id.files_checkBox11)
        mFilesCheckbox12 = tv.findViewById(R.id.files_checkBox12)
        mFilesCheckbox13 = tv.findViewById(R.id.files_checkBox13)
        mFilesCheckbox14 = tv.findViewById(R.id.files_checkBox14)
        mFilesCheckbox15 = tv.findViewById(R.id.files_checkBox15)
        mFilesCheckbox16 = tv.findViewById(R.id.files_checkBox16)
        mFilesCheckbox17 = tv.findViewById(R.id.files_checkBox17)
        mFilesCheckbox18 = tv.findViewById(R.id.files_checkBox18)
        mFilesCheckbox19 = tv.findViewById(R.id.files_checkBox19)
        mFilesCheckbox20 = tv.findViewById(R.id.files_checkBox20)

        onResume()
        return tv
    }

    //重写onResume页面
    override fun onResume() {
        super.onResume()
        //获取焦点，界面可见时候执行刷新
        if (isVisible) {
            FileisDirexist(mFilesCheckbox,"/storage/emulated/0/.com.taobao.dp")
            FileisDirexist(mFilesCheckbox2,"/storage/emulated/0/.zp")
            FileisDirexist(mFilesCheckbox3,"/storage/emulated/0/OSSLog")
            FileisDirexist(mFilesCheckbox4,"/storage/emulated/0/.gs_fs6")
            FileisDirexist(mFilesCheckbox5,"/storage/emulated/0/.gs_fs0")
            FileisDirexist(mFilesCheckbox6,"/storage/emulated/0/.7934039a")
            FileisDirexist(mFilesCheckbox7,"/storage/emulated/0/.OAIDSystemConfig")
            FileisDirexist(mFilesCheckbox8,"/storage/emulated/0/.UTSystemConfig")
            FileisDirexist(mFilesCheckbox9,"/storage/emulated/0/.BD_SAPI_CACHE")
            FileisDirexist(mFilesCheckbox10,"/storage/emulated/0/TurbonetDiskCache")
            FileisDirexist(mFilesCheckbox11,"/storage/emulated/0/TurboNet")
            FileisDirexist(mFilesCheckbox12,"/storage/emulated/0/.seprivate")
            FileisDirexist(mFilesCheckbox13,"/storage/emulated/0/.vdevdir")
            FileisDirexist(mFilesCheckbox14,"/storage/emulated/0/com.quark.browser")
            FileisDirexist(mFilesCheckbox15,"/storage/emulated/0/Android/data/org.telegram.messenger.web/files/Telegram/Telegram Video")
            FileisDirexist(mFilesCheckbox16,"/storage/emulated/0/Android/data/org.telegram.messenger.web/files/Telegram/Telegram Images")
            FileisDirexist(mFilesCheckbox17,"/storage/emulated/0/com.baidu.netdisk")
            FileisDirexist(mFilesCheckbox18,"/storage/emulated/0/alipay")
            FileisDirexist(mFilesCheckbox19,"/storage/emulated/0/geetest")
            FileisDirexist(mFilesCheckbox20,"/storage/emulated/0/ttscache")
        }
    }

    fun getRoot() {
            Runtime.getRuntime().exec("su")
    }

    fun FileisDirexist(Checkbox: CheckBox, filePath: String) {
        if (UseFileUtils.isDirExists(filePath)) {
            Checkbox.setChecked(true)}else{Checkbox.setChecked(false)}
    }

}