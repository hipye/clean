package io.moriz.cleaner.ui.colormark

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import io.moriz.cleaner.R
import io.moriz.cleaner.utils.ImageDialog

class ColorMarkFragment : Fragment() {

    private lateinit var mImageView1: ImageView
    private lateinit var mImageView2: ImageView
    private lateinit var mImageView3: ImageView
    private lateinit var mImageView4: ImageView
    private lateinit var mImageView5: ImageView
    private lateinit var mImageView6: ImageView
    private lateinit var mImageView7: ImageView
    private lateinit var mImageView8: ImageView
    private lateinit var mImageView9: ImageView
    private lateinit var mImageView10: ImageView
    private lateinit var mImageView11: ImageView
    private lateinit var mImageView12: ImageView
    private lateinit var mImageView13: ImageView
    private lateinit var mImageView14: ImageView
    private lateinit var mImageView15: ImageView
    private lateinit var mImageView16: ImageView
    private lateinit var mImageView17: ImageView
    private lateinit var mImageView18: ImageView
    private lateinit var mImageView19: ImageView
    private lateinit var mImageView20: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_colormark,container,false)
        mImageView1 = v.findViewById(R.id.color_marked1)
        mImageView2 = v.findViewById(R.id.color_marked2)
        mImageView3 = v.findViewById(R.id.color_marked3)
        mImageView4 = v.findViewById(R.id.color_marked4)
        mImageView5 = v.findViewById(R.id.color_marked5)
        mImageView6 = v.findViewById(R.id.color_marked6)
        mImageView7 = v.findViewById(R.id.color_marked7)
        mImageView8 = v.findViewById(R.id.color_marked8)
        mImageView9 = v.findViewById(R.id.color_marked9)
        mImageView10 = v.findViewById(R.id.color_marked10)
        mImageView11 = v.findViewById(R.id.color_marked11)
        mImageView12 = v.findViewById(R.id.color_marked12)
        mImageView13 = v.findViewById(R.id.color_marked13)
        mImageView14 = v.findViewById(R.id.color_marked14)
        mImageView15 = v.findViewById(R.id.color_marked15)
        mImageView16 = v.findViewById(R.id.color_marked16)
        mImageView17 = v.findViewById(R.id.color_marked17)
        mImageView18 = v.findViewById(R.id.color_marked18)
        mImageView19 = v.findViewById(R.id.color_marked19)
        mImageView20 = v.findViewById(R.id.color_marked20)

//        mImageView1.setOnClickListener {
//            mImageView1.setDrawingCacheEnabled(true)
//            val ImageDialog = ImageDialog(
//                activity,
//                R.animator.dialog,
//                0,
//                -300,
//                mImageView1.getDrawingCache()
//            ).show()
//            val builder = MaterialAlertDialogBuilder(this.requireContext())
//            builder.setTitle("当前标签主要色")
//            builder.setMessage("是否复制当前色彩标签的主要色")
//            builder.setNeutralButton("色彩一") { dialog, which ->
//                val clipboardManager = requireActivity().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
//                val clipData = ClipData.newPlainText("Label", "#174994")
//                clipboardManager.setPrimaryClip(clipData)
//                showToast(this.requireContext(),"复制色块一成功")
//            }
//            builder.setNegativeButton("色彩二") { dialog, which ->
//                val clipboardManager = requireActivity().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
//                val clipData = ClipData.newPlainText("Label", "#B179A1")
//                clipboardManager.setPrimaryClip(clipData)
//                showToast(this.requireContext(),"复制色块二成功")
//            }
//            builder.setPositiveButton("色彩三") { dialog, which ->
//                val clipboardManager = requireActivity().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
//                val clipData = ClipData.newPlainText("Label", "#F5B6A5")
//                clipboardManager.setPrimaryClip(clipData)
//                showToast(this.requireContext(),"复制色块三成功")
//            }
//            builder.show()
//        }

        setImageViewClickListener(mImageView1,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView2,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView3,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView4,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView5,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView6,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView7,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView8,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView9,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView10,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView11,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView12,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView13,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView14,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView15,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView16,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView17,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView18,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView19,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        setImageViewClickListener(mImageView20,"#2B3467\n#BAD7E9\n#FCFFE7\n#EB455F")
        return v
    }
    fun setImageViewClickListener(imageView: ImageView,text: String) {
        imageView.setOnClickListener {
            imageView.setDrawingCacheEnabled(true)
            val ImageDialog = ImageDialog(
                activity,
                R.animator.dialog,
                0,
                -300,
                imageView.getDrawingCache()
            ).show()
            val clipboardManager = requireActivity().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("Label", text)
            clipboardManager.setPrimaryClip(clipData)
            Toast.makeText(context,"复制色块到剪切板成功",Toast.LENGTH_SHORT).show()
        }
    }

}