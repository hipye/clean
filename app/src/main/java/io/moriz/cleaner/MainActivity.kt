package io.moriz.cleaner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Message
import android.util.Log
import android.widget.Button
import android.widget.TextView
import io.moriz.cleaner.ui.core.FileCoreActivity
import io.moriz.cleaner.ui.selector.FileSelectMultiImageActivity
import io.moriz.cleaner.ui.selector.FileSelectSingleImageActivity
import io.moriz.cleaner.utils.PermissionManager
import io.moriz.norutil.*
import java.io.*

class MainActivity : AppCompatActivity() {

    var filePath: String? = null
    var fileName: String? = null
    var textView: TextView? = null
    var inputStreamRawTxt: InputStream? = null
    var handler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            textView!!.text = msg.obj.toString()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initData()
        initEvent()
        StoragePermissionUtil.registerForActivityResult(this) //使用函数为这个Activity注册并返回结果
        StoragePermissionUtil.requestStoragePermission(this){}
        //FileUtils.creatFile("/storage/emulated/0/Document/","test")  只能使用这个地址来使用内部存储的路径  /storage/emulated/0/
//        PermissionManager.requestStoragePermission(this)
        var mabutton = findViewById<Button>(R.id.MAbutton)
        mabutton.setOnClickListener {
            val intent =  Intent(this, FileSelectSingleImageActivity::class.java)
            startActivity(intent)
        }
        var mabutton2 = findViewById<Button>(R.id.MAbutton2)
        mabutton2.setOnClickListener {
            val intent =  Intent(this, FileSelectMultiImageActivity::class.java)
            startActivity(intent)
        }
        var rmfbutton = findViewById<Button>(R.id.RMFbutton)
        rmfbutton.setOnClickListener {
            showToast(this,"删除垃圾文件")
            UseFileUtils.delDir("/storage/emulated/0/.com.taobao.dp")
            UseFileUtils.delDir("/storage/emulated/0/.zp")
            UseFileUtils.delDir("/storage/emulated/0/OSSLog")
            UseFileUtils.delDir("/storage/emulated/0/.gs_fs6")
            UseFileUtils.delDir("/storage/emulated/0/.gs_fs0")
            UseFileUtils.delDir("/storage/emulated/0/.7934039a")
            UseFileUtils.delDir("/storage/emulated/0/.OAIDSystemConfig")
            UseFileUtils.delDir("/storage/emulated/0/.UTSystemConfig")
            UseFileUtils.delDir("/storage/emulated/0/.BD_SAPI_CACHE")
            UseFileUtils.delDir("/storage/emulated/0/TurbonetDiskCache")
            UseFileUtils.delDir("/storage/emulated/0/TurboNet")
            UseFileUtils.delDir("/storage/emulated/0/.seprivate")
            //UseFileUtils.delDir("/storage/emulated/0/Android/data/org.telegram.messenger.web/files/Telegram/Telegram Video")
            UseFileUtils.delDir("/storage/emulated/0/.vdevdir")
            //UseFileUtils.delDir("/storage/emulated/0/Android/data/org.telegram.messenger.web/files/Telegram/Telegram Images")
        }
    }

    private fun initView() {
        textView = findViewById(R.id.matextview)
    }//寻找绑定TextView控件

    private fun initData() {
        filePath = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).absolutePath
                + "/")
        fileName = "Cleanernortic"
        //        inputStreamRawTxt =getResources().openRawResource(R.raw.printf);
    }//定义要寻找的文件地址

    private fun initEvent() {

        //判断是否有该文件夹，没有则创建一个，生成文件夹之后，再生成文件，不然会出错
        //在线程中执行耗时操作
        Thread { //判断是否有该文件，没有则创建一个,并返回true
            if (createNewFile(filePath, fileName)) {
                //返回true说明创建了新的文件，则写入预设内容，
                //返回false，则说明文件已存在，不需要写入（也不会进入该判断）

                //将输入流转换成字符串，写入文件
                val rawTxtString = Stream2String(inputStreamRawTxt)
                writeTxtToFile(rawTxtString, filePath, fileName)
            }

            //再将文件读取为字符串
            val text = ReadTxtFromSDCard(fileName)
            //显示出来
            val message = Message.obtain(handler)
            message.obj = text
            handler.sendMessage(message)
        }.start()
    }

    // 生成文件
    fun createNewFile(filePath: String?, fileName: String?): Boolean {
        var file: File? = null
        try {
            file = File(filePath + fileName)
            return if (!file.exists()) {
                file.createNewFile()
                true
            } else {
                false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    // 将字符串写入到文本文件中
    fun writeTxtToFile(
        stringContent: String,
        filePath: String?, fileName: String?
    ) {
        val strFilePath = filePath + fileName
        // 每次写入时，都换行写
        val strContent = """
             $stringContent
             
             """.trimIndent()
        val file = File(strFilePath)
        try {
            val raf = RandomAccessFile(file, "rwd")
            raf.seek(file.length())
            raf.write(strContent.toByteArray())
            raf.close()
        } catch (e: Exception) {
            Log.e("TestFile", "Error on write File:$e")
        }
    }

    private fun Stream2String(`is`: InputStream?): String {
        //强制缓存大小为16KB，一般Java类默认为8KB
        val reader = BufferedReader(
            InputStreamReader(`is`), 16 * 1024
        )
        val sb = StringBuilder()
        var line: String? = null
        try {
            while (reader.readLine().also { line = it } != null) {  //处理换行符
                sb.append(
                    """
    $line
    
    """.trimIndent()
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return sb.toString()
    }

    //这是这篇的重点，按ctrl+f关注input的操作
    private fun ReadTxtFromSDCard(filename: String?): String {
        val sb = StringBuilder("")
        //判断是否有读取权限
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {

            //打开文件输入流
            try {
                val input = FileInputStream(filePath + filename)
                val temp = ByteArray(1024)
                var len = 0
                //读取文件内容:
                while (input.read(temp).also { len = it } > 0) {
                    sb.append(String(temp, 0, len))
                    L.d(sb.toString())
                }
                //关闭输入流
                input.close()
            } catch (e: IOException) {
                Log.e("ReadTxtFromSDCard", "ReadTxtFromSDCard")
                e.printStackTrace()
            }
        }
        return sb.toString()
    }
}